#Exercise 11:
#Ruby program that, when given an array:
collection = [12, 23, 456, 123, 4579]
##collection = Array.new(rand(100)) { rand(100) }
# prints each number, and tells you whether it is odd or even.

def odd_or_even(num)
num % 2 == 0 ? "even" : "odd"
end

collection.each do |number|
  puts "The number #{number} is #{odd_or_even(number)}."
end

