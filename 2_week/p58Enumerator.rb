short_enum = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].to_enum
long_enum = ('a'..'z').to_enum

loop do
  puts " #{short_enum.next} - #{long_enum.next} "
end
