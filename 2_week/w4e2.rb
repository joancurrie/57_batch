 def random(number)
        rand(0) * number
 end

class Rectangle
  def initialize( width, length )
    @area = width*length
    @perimeter = 2*( width + length )
  end

  def area
    @area
  end
  def perimeter
    @perimeter
  end
=begin
  def random(number)

      rand(0) * number

  end
=end
end

r = Rectangle.new(23.45, 34.67)

puts "Area is = #{r.area}"
puts "Perimeter is = #{r.perimeter}"

puts "I have the following 10 rectangles:"
puts "Rectangle #, Width, Length, area"
#=begin
rectangles = 10.times.map do |i|

  width, length = random(20), random(20)
  puts "%11d %6.2f, %6.2f, %8.2f" % [i.next, width, length, width * length ]
  Rectangle.new(random(20), random(20))
end

puts "I should be able to get a total of all the areas, let me try that now."
puts "%38s" % 'sum'
puts "%38f" % rectangles.reduce(0) {|total, r| total + r.area}
#=end
