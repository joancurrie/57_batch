def leap_year?(yr)

  raise ArgumentError, "yr (='#{yr}) must be non-negative" unless yr >= 0

  if  ( yr == 1900 ) || ( yr == 2005 )
    puts "The common year is #{ 60 * 24 * 365 } minutes."
  end
  if ( yr == 2000 ) || ( yr == 2004 )
    puts "The leap year is #{ 60 * 24 * 366 } minutes."
  end

  if yr ==  0
    false
  elsif ( yr % 400 )  == 0
    true
  elsif ( yr % 100 )  == 0
    false
  else
      ( yr % 4 ) == 0
#    true
  end

end

print "Year is "
STDOUT.flush
year = gets.chomp.to_i

puts leap_year?(year)
