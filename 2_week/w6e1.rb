#accessor method
class Person
  def initialize(balance)
    @balance = balance
  end

  attr_reader :balance
end

person = Person.new(123.45)

puts person.balance
