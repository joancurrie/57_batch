﻿def open_file_and_get_lines(file_name)
  f = File.open(file_name)
  a = f.readlines
#f.close
#a
end
 
def find_line_to_be_replaced(lines)
  lines.each_with_index do |line, index|
    tmp = line.split(" ")
    return index if tmp.include?("word")
  end
  ##puts "Word not found!"
  exit
end
 
def clear_file_and_rewrite(file_name, lines)
  f = File.open(file_name, "w+")
  lines.each {|line| f.puts line}
#  f.close
end
 
print "Enter file name : "
fname = gets.chomp

puts fname
lines_arr = open_file_and_get_lines(fname)
index = find_line_to_be_replaced(lines_arr)
lines_arr[index] = "text text inserted word text text"
clear_file_and_rewrite(fname, lines_arr)

