# p048accessor.rb  
# First WITHout ACCESSOR methods
class Song  
  def initialize(name, artist)
    @name     = name
    @artist   = artist
#wrong:    puts @name
#wrong:    puts name
  end  
#=begin
  def name
    @name
  end
  def artist
    @artist
  end
#=end
end

song = Song.new("Brazil", "Ivete Sangalo")  
puts song.name  
puts song.artist  
#                                                                                                         
#                                                                                                             # Now, with accessor methods  
#                                                                                                                 class Song  
#                                                                                                                       def initialize(name, artist)  
#                                                                                                                               @name     = name  
#                                                                                                                                       @artist   = artist  
#                                                                                                                                             end  
#                                                                                                                                                   attr_reader :name, :artist  # create reader only  
#                                                                                                                                                         # For creating reader and writer methods  
#                                                                                                                                                               # attr_accessor :name  
#                                                                                                                                                                     # For creating writer methods  
#                                                                                                                                                                           # attr_writer :name  
#                                                                                                                                                                                 
#                                                                                                                                                                                     end  
#                                                                                                                                                                                           
#                                                                                                                                                                                               song = Song.new("Brazil", "Ivete Sangalo")  
#                                                                                                                                                                                                   puts song.name  
#                                                                                                                                                                                                       puts song.artist  
