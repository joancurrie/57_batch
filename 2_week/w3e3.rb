
#Display current working directory
#What current working directory does it think it is in?
#http://code.tutsplus.com/tutorials/ruby-for-newbies-working-with-directories-and-files--net-18810
puts Dir.pwd

#create a new directory tmp under your working directory:
Dir.mkdir("tmp")

puts Dir.pwd

#change directory within the Ruby program:
Dir.chdir("tmp")

#display your current working directory:
puts
#cd ..
#delete the tmp directory:
Dir.chdir("..")
Dir.delete("tmp")



