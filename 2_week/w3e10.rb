collection = [1, 2, 3, 4, 5]

sum = 0
collection.each { |c| sum = sum + c }

puts  "The integer sum of 1 to 5 is #{sum}."
