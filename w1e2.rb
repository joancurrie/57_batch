#http://www.ruby-doc.org/core-2.1.5/Kernel.html
# format(format_string [, arguments...] ) → string
#
# Returns the string resulting from applying format_string to any additional arguments.
# Within the format string, any characters other than format sequences are copied to the result.

# The syntax of a format sequence is follows.
# %[flags][width][.precision]type

# Flag     | Applies to    | Meaning
# 0 (zero) | decimal number| Pad with zeros, not spaces.
# 5 is how many spaces for the whole format string.
# d is a decimal number.

# output is:
# 00123
puts "%05d" % 123
