y = false
z = true

# = has higher operator than or.
#       true  or true  = true        true and true  = true
#       true  or false = true        true and false = true
#       false or true  = true        false and true = true
#       false or false = false       false and false= false
#
# x = y is evaluated before the or.
# Therefore x = (y = false)
#           x = false
x = y or z
#          (x = false) or z
#          (x = false) or true
# Therefore x = false.
puts x

(x = y) or z
#(x = false) or (z = true)
#Therefore x = false.
puts x

x = (y or z)
#x = (false or true)
#Therefore x = true
puts x
